# flexibee-reports
Tiskové sestavy pro FlexiBee

<a href="https://charlieblog.eu/flexibee-modre-tiskopisy">Domovská stránka projektu</a>

<h2>Tisková sestava vydané faktury</h2>
<img src="https://charlieblog.eu/clanky/flexibee_blue_tiskopisy/faktura-blue-cz.png" />

<h2>Tisková sestava zjednodušený daňový doklad (58mm)</h2>
<img src="https://charlieblog.eu/clanky/flexibee_blue_tiskopisy/prodejka-blue-cz.png" />
